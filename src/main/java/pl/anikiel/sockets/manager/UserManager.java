package pl.anikiel.sockets.manager;

import pl.anikiel.sockets.domain.Message;
import pl.anikiel.sockets.domain.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserManager {
    private final FileManager fileManager;
    private final List<User> registeredUsers = new ArrayList<>();
    private User loggedUser;

    public UserManager(FileManager fileManager) throws IOException {
        this.fileManager = fileManager;
    }

    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    public List<User> getRegisteredUsers() {
        return registeredUsers;
    }

    public void registerUser(User user) {
        registeredUsers.add(user);
    }

    public void unregisterUser(User user) {
        registeredUsers.remove(user);
    }

    public void registerUsersFromFiles() throws IOException {
        List<User> users = fileManager.getAllUsersFromFiles();

        for (User user : users) {
            registerUser(user);
        }
    }

    public boolean registerUser(String login, String password) throws IOException {
        if ("".equals(login) || "".equals(password) || userExists(login)) {
            return false;
        }

        User newUser = createUser(login, password);
        registerUser(newUser);

        return true;
    }

    public boolean loginUser(String login, String password) {
        if (checkUser(login, password)) {
            User loggedUser = findUser(login);
            setLoggedUser(loggedUser);

            return true;
        }
        return false;
    }

    public void logoutUser() {
        setLoggedUser(null);
    }

    public boolean deleteUser(String login) {
        User user = findUser(login);

        if (user == null || loggedUser == null || user.isAdmin()) {
            return false;
        }

        if (!loggedUser.isAdmin() && loggedUser != user) {
            return false;
        }

        if (!loggedUser.isAdmin()) {
            logoutUser();
        }

        unregisterUser(user);

        return fileManager.deleteUserFile(user);
    }

    public boolean userExists(String login) {
        User user = findUser(login);
        return user != null;
    }

    public User findUser(String login) {
        for (User user : registeredUsers) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }

    private User createUser(String login, String password) throws IOException {
        User newUser = new User(login, password);
        fileManager.updateUserFile(newUser);

        return newUser;
    }

    private boolean checkUser(String login, String password) {
        if (!userExists(login)) {
            System.out.println("This login is not registered!");
            return false;
        }
        if (!isValidPassword(login, password)) {
            System.out.println("Wrong password!");
            return false;
        }
        return true;
    }

    private boolean isValidPassword(String login, String password) {
        for (User user : registeredUsers) {
            if (user.getLogin().equals(login)) {
                if (user.getPassword().equals(password)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void writeUserMessage(Message message, String recipient) throws IOException {
        User user = findUser(recipient);

        if (user == null) {
            return;
        }

        user.addMessage(message);
        fileManager.updateUserFile(user);
    }

    public void readUserMessage(User user, Message message) throws IOException {
        user.readMessage(message);
        fileManager.updateUserFile(user);
    }

    public void deleteUserMessage(User user, Message message) throws IOException {
        user.deleteMessage(message);
        fileManager.updateUserFile(user);
    }
}