package pl.anikiel.sockets.manager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import pl.anikiel.sockets.domain.Message;
import pl.anikiel.sockets.domain.User;
import pl.anikiel.sockets.server.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CommandManager {
    private final Server server;
    private final UserManager userManager = new UserManager(new FileManager());
    private PrintWriter out;
    private BufferedReader in;
    private final Instant startTime = Instant.now();
    private final ObjectMapper objectMapper = new ObjectMapper();

    public CommandManager(Server server) throws IOException {
        this.server = server;
        userManager.registerUsersFromFiles();
        start();
    }

    private void start() throws IOException {
        server.start();
        out = new PrintWriter(server.getClientSocket().getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(server.getClientSocket().getInputStream()));
    }

    private void stop() throws IOException {
        in.close();
        out.close();
        server.stop();
    }

    public void showMainMenu() throws IOException {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("MAIN MENU", "Available commands:");
        jsonMap.put("login", "Login with user and password");
        jsonMap.put("signup", "Create an account");
        jsonMap.put("server", "Show server menu");

        while (server.isActive()) {
            sendCommand(jsonMap, "true");
            switch (in.readLine()) {
                case "login":
                    loginUser();
                    break;
                case "signup":
                    registerUser();
                    break;
                case "server":
                    showServerMenu();
                    break;
                default:
                    showCommand("Command unknown.", "false");
            }
        }
    }

    private void showUserMenu() throws IOException {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("USER MENU", "Available commands:");
        jsonMap.put("message", "Manage messages");
        jsonMap.put("delete", "Delete account");
        jsonMap.put("back", "Back to previous menu");

        boolean exit = false;

        while (!exit) {
            sendCommand(jsonMap, "true");
            switch (in.readLine()) {
                case "message":
                    messageMenu();
                    break;
                case "delete":
                    deleteUser(userManager.getLoggedUser().getLogin());
                    exit = true;
                    break;
                case "back":
                    logoutUser();
                    exit = true;
                    break;
                default:
                    showCommand("Command unknown.", "false");
            }
        }
    }

    private void showAdminMenu() throws IOException {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("ADMIN MENU", "Available commands:");
        jsonMap.put("message", "Manage messages");
        jsonMap.put("manage", "Manage users");
        jsonMap.put("stop", "Stop server and client");
        jsonMap.put("back", "Back to previous menu");

        boolean exit = false;

        while (!exit) {
            sendCommand(jsonMap, "true");
            switch (in.readLine()) {
                case "message":
                    messageMenu();
                    break;
                case "manage":
                    showManageUsersMenu();
                    break;
                case "stop":
                    stop();
                    exit = true;
                    break;
                case "back":
                    logoutUser();
                    exit = true;
                    break;
                default:
                    showCommand("Command unknown.", "false");
            }
        }
    }

    private void showManageUsersMenu() throws IOException {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("MANAGE USERS MENU", "Available commands:");
        jsonMap.put("users", "Show registered users");
        jsonMap.put("delete", "Delete user");
        jsonMap.put("back", "Back to previous menu");

        boolean exit = false;

        while (!exit) {
            sendCommand(jsonMap, "true");
            switch (in.readLine()) {
                case "users":
                    showRegisteredUsers();
                    break;
                case "delete":
                    deleteUser();
                    exit = true;
                    break;
                case "back":
                    exit = true;
                    break;
                default:
                    showCommand("Command unknown.", "false");
            }
        }
    }

    private void messageMenu() throws IOException {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("MESSAGE MENU", "Available commands:");
        jsonMap.put("read", "Read message");
        jsonMap.put("write", "Write message");
        jsonMap.put("delete", "Delete message");
        jsonMap.put("back", "Back to previous menu");

        boolean exit = false;

        while (!exit) {
            sendCommand(jsonMap, "true");

            switch (in.readLine()) {
                case "read":
                    readMessage();
                    break;
                case "write":
                    writeMessage();
                    break;
                case "delete":
                    deleteMessage();
                    break;
                case "back":
                    exit = true;
                    break;
                default:
                    showCommand("Command unknown.", "false");
            }
        }
    }

    private void showServerMenu() throws IOException {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("SERVER MENU", "Available commands:");
        jsonMap.put("uptime", "Return server running time");
        jsonMap.put("info", "Return server version and creation date");
        jsonMap.put("help", "Return list of available commands");
        jsonMap.put("back", "Back to previous menu");

        boolean exit = false;

        while (!exit) {
            sendCommand(jsonMap, "true");

            switch (in.readLine()) {
                case "uptime":
                    showUptime();
                    break;
                case "info":
                    showInfo();
                    break;
                case "help":
                    showHelp();
                    break;
                case "back":
                    exit = true;
                    break;
                default:
                    showCommand("Command unknown.", "false");
            }
        }
    }

    private void showUptime() throws JsonProcessingException {
        Duration lifeTimeDuration = Duration.between(startTime, Instant.now());
        long seconds = lifeTimeDuration.getSeconds();

        long HH = seconds / 3600;
        long MM = (seconds % 3600) / 60;
        long SS = seconds % 60;

        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("hours", Long.toString(HH));
        jsonMap.put("minutes", Long.toString(MM));
        jsonMap.put("seconds", Long.toString(SS));

        sendCommand(jsonMap, "false");
    }

    private void showInfo() throws JsonProcessingException {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("server version", server.getServerVersion());
        jsonMap.put("creation date", server.getCreationDate());

        sendCommand(jsonMap, "false");
    }

    private void showHelp() throws JsonProcessingException {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("uptime", "Return server running time");
        jsonMap.put("info", "Return server version and creation date");
        jsonMap.put("help", "Return list of available commands");

        sendCommand(jsonMap, "false");
    }

    private void showRegisteredUsers() throws JsonProcessingException {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("USERS", "List of registered users:");

        List<User> registeredUsers = userManager.getRegisteredUsers();

        int i = 1;
        for (User user : registeredUsers) {
            jsonMap.put(Integer.toString(i), user.getLogin());
            i++;
        }

        sendCommand(jsonMap, "false");
    }

    private void showCommand(String command, String isResponseRequired) throws IOException {
        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("command", command);

        sendCommand(jsonMap, isResponseRequired);
    }

    private void sendCommand(Map<String, String> command, String isResponseRequired) throws JsonProcessingException {
        ObjectNode jsonNode = objectMapper.createObjectNode();

        jsonNode.put("responseRequired", isResponseRequired);

        for (Map.Entry<String, String> prop : command.entrySet()) {
            jsonNode.put(prop.getKey(), prop.getValue());
        }

        String json = objectMapper.writeValueAsString(jsonNode);
        out.println(json);
    }

    private String receiveAnswer() throws IOException {
        return in.readLine();
    }

    private void loginUser() throws IOException {
        showCommand("Enter login:", "true");
        String login = receiveAnswer();
        showCommand("Input password:", "true");
        String password = receiveAnswer();

        boolean success = userManager.loginUser(login, password);

        if (success) {
            showCommand("Authorization have been successful.", "false");
            System.out.println("Authorization have been successful!");

            if (userManager.getLoggedUser().isAdmin()) {
                showAdminMenu();
            } else {
                showUserMenu();
            }
        } else {
            showCommand("Wrong authorization data!", "false");
        }
    }

    private void logoutUser() throws IOException {
        userManager.logoutUser();

        showCommand("User logged out.", "false");
        System.out.println("User logged out.");
    }

    private void registerUser() throws IOException {
        showCommand("Enter login:", "true");
        String login = receiveAnswer();
        showCommand("Input password:", "true");
        String password = receiveAnswer();

        boolean success = userManager.registerUser(login, password);

        if (success) {
            showCommand("Account created.", "false");
            System.out.println("Account created.");
        } else {
            showCommand("This login is already registered or login data not allowed!", "false");
        }
    }

    private void deleteUser() throws IOException {
        showCommand("Enter login to be deleted:", "true");
        String login = receiveAnswer();

        if (userManager.userExists(login)) {
            if (userManager.findUser(login).isAdmin()) {
                showCommand("You cannot delete admin account!", "false");
            } else {
                deleteUser(login);
            }
        } else {
            showCommand("Account not found!", "false");
        }
    }

    private void deleteUser(String login) throws IOException {
        userManager.deleteUser(login);

        showCommand("Account deleted.", "false");
        System.out.println("Account deleted.");
    }

    private void writeMessage() throws IOException {
        showCommand("Enter recipient:", "true");
        String recipient = receiveAnswer();

        if (userManager.userExists(recipient)) {
            showCommand("Write topic:", "true");
            String topic = receiveAnswer();

            showCommand("Write message:", "true");
            String textMessage = receiveAnswer();

            try {
                Message message = new Message(textMessage, topic, userManager.getLoggedUser().getLogin());
                userManager.writeUserMessage(message, recipient);

                showCommand("Message sent.", "false");
                System.out.println("Message sent.");
            } catch (IllegalStateException exception) {
                showCommand("Receiver's mailbox is full! Message not sent.", "false");
                System.out.println("Receiver's mailbox is full! Message not sent");
            } catch (IllegalArgumentException exception) {
                showCommand("Maximum message length exceeded! Message not sent.", "false");
                System.out.println("Maximum message length exceeded! Message not sent.");
            }
        } else {
            showCommand("Receiver doesn't exist.", "false");
            System.out.println("Receiver doesn't exist.");
        }
    }

    private void readMessage() throws IOException {
        Message message = getMessage("read");

        if (message == null) {
            return;
        }

        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("topic", message.getTopic());
        jsonMap.put("from", message.getFrom());
        jsonMap.put("message", message.getMessage());

        sendCommand(jsonMap, "false");

        userManager.readUserMessage(userManager.getLoggedUser(), message);

        showCommand("Message read.", "false");
        System.out.println("Message read.");
    }

    private void deleteMessage() throws IOException {
        Message message = getMessage("delete");

        if (message == null) {
            return;
        }

        userManager.deleteUserMessage(userManager.getLoggedUser(), message);

        showCommand("Message deleted.", "false");
        System.out.println("Message deleted.");
    }

    private Message getMessage(String action) throws IOException {
        List<Message> messages = userManager.getLoggedUser().getMessages();

        if (messages.size() == 0) {
            showCommand("Mailbox is empty.", "false");
            System.out.println("Mailbox is empty.");
            return null;
        }

        Map<String, String> jsonMap = new LinkedHashMap<>();
        jsonMap.put("NR", "TOPIC");

        int nrMessage = 1;
        for (Message message : messages) {
            jsonMap.put(Integer.toString(nrMessage), message.getTopic());
            nrMessage++;
        }
        sendCommand(jsonMap, "false");

        showCommand("Choose NR of message you want to " + action + ":", "true");
        int nrTopic = Integer.parseInt(receiveAnswer());

        if (nrTopic < 1 || nrTopic > nrMessage) {
            showCommand("Message with this NR doesn't exist!", "false");
            System.out.println("Message with this NR doesn't exist!");
            return null;
        }

        return messages.get(nrTopic - 1);
    }

    public void run() throws IOException {
        showMainMenu();
    }
}