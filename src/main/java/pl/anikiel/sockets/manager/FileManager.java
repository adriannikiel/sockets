package pl.anikiel.sockets.manager;

import com.fasterxml.jackson.databind.ObjectMapper;
import pl.anikiel.sockets.domain.User;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileManager {
    private final String jsonUserFolder;
    private static final String DEFAULT_JSON_USER_FOLDER = "src/main/resources/json/users/";
    private final ObjectMapper objectMapper = new ObjectMapper();

    public FileManager() {
        this.jsonUserFolder = DEFAULT_JSON_USER_FOLDER;
    }

    public FileManager(String jsonUserFolder) {
        this.jsonUserFolder = jsonUserFolder;
    }

    public List<User> getAllUsersFromFiles() throws IOException {
        File jsonFolder = new File(jsonUserFolder);

        List<File> userFiles = Files.walk(Paths.get(jsonFolder.toURI()))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());

        List<User> users = new ArrayList<>();

        for (File userFile : userFiles) {
            User user = objectMapper.readValue(userFile, User.class);
            users.add(user);
        }
        return users;
    }

    public void updateUserFile(User user) throws IOException {
        File userFile = new File(jsonUserFolder + user.getLogin() + ".json");
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(userFile, user);
    }

    public boolean deleteUserFile(User user) {
        File userFile = new File(jsonUserFolder + user.getLogin() + ".json");
        return userFile.delete();
    }
}
