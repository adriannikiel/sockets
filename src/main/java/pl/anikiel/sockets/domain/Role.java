package pl.anikiel.sockets.domain;

public enum Role {
    ADMIN, USER;
}