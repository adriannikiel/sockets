package pl.anikiel.sockets.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class User {
    private final static int MAX_UNREAD_MESSAGES = 5;
    private String login;
    private String password;
    private Role role;
    private List<Message> messages = new ArrayList<>();

    public User() {
    }

    public User(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
        this.role = Role.USER;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    @JsonIgnore
    public boolean isAdmin() {
        return role == Role.ADMIN;
    }

    public boolean addMessage(Message message) {
        if (!isAdmin() && countUnreadMessages() >= MAX_UNREAD_MESSAGES) {
            throw new IllegalStateException("Max unread messages exceeded");
        }
        return messages.add(message);
    }

    public boolean readMessage(Message message) {
        if (messages.contains(message)) {
            messages.stream()
                    .filter(m -> m.equals(message))
                    .findFirst().get()
                    .setRead(true);

            message.setRead(true);
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteMessage(Message message) {
        if (messages.contains(message)) {
            messages.remove(message);
            return true;
        } else {
            return false;
        }
    }

    private long countUnreadMessages() {
        return messages.stream()
                .filter(m -> !m.isRead())
                .count();
    }
}