package pl.anikiel.sockets.domain;

import java.util.Objects;

public class Message {
    private final static int MAX_LENGTH_OF_MESSAGE = 255;
    private String topic;
    private String message;
    private String from;
    private boolean read;

    public Message() {
    }

    public Message(String message, String topic, String from) {
        setMessage(message);
        this.topic = topic;
        this.from = from;
        this.read = false;
    }

    public String getMessage() {
        return message;
    }

    public String getFrom() {
        return from;
    }

    public String getTopic() {
        return topic;
    }

    public boolean isRead() {
        return read;
    }

    public void setMessage(String message) {
        if (message.length() > MAX_LENGTH_OF_MESSAGE) {
            throw new IllegalArgumentException("Max length exceeded");
        }
        this.message = message;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return read == message1.read && Objects.equals(topic, message1.topic) && Objects.equals(message, message1.message) && Objects.equals(from, message1.from);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topic, message, from, read);
    }
}