package pl.anikiel.sockets.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    void shouldReturnTrueForAdminWhenUserIsSetAsAdmin() {
        //given
        User user = new User("login", "password", Role.ADMIN);

        //when
        //then
        assertTrue(user.isAdmin());
    }

    @Test
    void shouldReturnFalseForAdminWhenUserIsSetAsNormalUser() {
        //given
        User user = new User("login", "password", Role.USER);

        //when
        //then
        assertFalse(user.isAdmin());
    }

    @Test
    void shouldReturnFalseForAdminWhenUserIsNotSet() {
        //given
        User user = new User("login", "password");

        //when
        //then
        assertThat(user.isAdmin()).isFalse();
    }

    @Test
    void shouldAllowToAddNewMessageWhenNormalUserHasLessThenFiveUnreadMessages() {
        //given
        User user = getUserWithMessages(5, false);

        //then
        assertThat(user.getMessages().size()).isEqualTo(5);
        assertThat(user.getMessages().get(4).getMessage()).isEqualTo("message5");
    }

    @Test
    void shouldAllowToAddNewMessageWhenAdminHasMoreThenFiveUnreadMessages() {
        //given
        User admin = getUserWithMessages(5, true);

        //when
        admin.addMessage(new Message("message6", "topic6", "from6"));

        //then
        assertThat(admin.getMessages().size()).isEqualTo(6);
        assertThat(admin.getMessages().get(5).getMessage()).isEqualTo("message6");
    }

    @Test
    void shouldThrowExceptionWhenNormalUserHasMoreThenFiveUnreadMessages() {
        //given
        User user = getUserWithMessages(5, false);

        //when
        //then
        assertThrows(IllegalStateException.class, () -> user.addMessage(new Message("message", "topic", "from")));
    }

    @Test
    void shouldDeleteMessageWhenEqualMessageFound() {
        //given
        Message message1 = new Message("message", "topic", "from");
        Message message2 = new Message("message", "topic", "from");

        User user = new User("login", "password");
        user.addMessage(message1);

        //when
        boolean result = user.deleteMessage(message2);

        //then
        assertThat(user.getMessages()).isEmpty();
        assertTrue(result);
    }

    @Test
    void shouldNotDeleteMessageWhenMessageNotFound() {
        //given
        Message message1 = new Message("message1", "topic", "from");
        Message message2 = new Message("message2", "topic", "from");

        User user = new User("login", "password");
        user.addMessage(message1);

        //when
        boolean result = user.deleteMessage(message2);

        //then
        assertThat(user.getMessages().size()).isEqualTo(1);
        assertFalse(result);
    }

    @Test
    void shouldReadMessageWhenEqualMessageFound() {
        //given
        Message message1 = new Message("message", "topic", "from");
        Message message2 = new Message("message", "topic", "from");

        User user = new User("login", "password");
        user.addMessage(message1);

        //when
        boolean result = user.readMessage(message2);

        //then
        assertThat(user.getMessages().get(0).isRead()).isTrue();
        assertTrue(result);
    }

    @Test
    void shouldNotReadMessageWhenMessageNotFound() {
        //given
        Message message1 = new Message("message1", "topic", "from");
        Message message2 = new Message("message2", "topic", "from");

        User user = new User("login", "password");
        user.addMessage(message1);

        //when
        boolean result = user.readMessage(message2);

        //then
        assertThat(user.getMessages().get(0).isRead()).isFalse();
        assertFalse(result);
    }

    private static User getUserWithMessages(int numberOfMessages, boolean isAdmin) {
        User user;

        if (isAdmin) {
            user = new User("login", "password", Role.ADMIN);
        } else {
            user = new User("login", "password");
        }

        for (int i = 0; i < numberOfMessages; i++) {
            user.addMessage(new Message("message" + (i + 1), "topic" + (i + 1), "from" + (i + 1)));
        }
        return user;
    }
}