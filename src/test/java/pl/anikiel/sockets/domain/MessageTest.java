package pl.anikiel.sockets.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class MessageTest {
    @Test
    void shouldThrowExceptionWhenGivenMessageIsLongerThanMaxLength() {
        //given
        String longMessage = "The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. " +
                "Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. " +
                "Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. " +
                "Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack.";
        Message message = new Message();

        //when
        //then
        assertThrows(IllegalArgumentException.class, () -> message.setMessage(longMessage));
    }

    @Test
    void shouldSetMessageWhenMessageLengthIsNotExceeded() {
        //given
        Message message = new Message();

        //when
        message.setMessage("message");

        //when
        assertThat(message.getMessage()).isEqualTo("message");
    }
}