package pl.anikiel.sockets.manager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.anikiel.sockets.domain.Message;
import pl.anikiel.sockets.domain.Role;
import pl.anikiel.sockets.domain.User;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;

@ExtendWith(MockitoExtension.class)
class UserManagerTest {

    private UserManager userManager;
    @Mock
    private FileManager fileManager;

    @BeforeEach
    void setUp() throws IOException {
        userManager = new UserManager(fileManager);
    }

    @Test
    void shouldReturnEmptyListWhenNoUserFoundFromFiles() throws IOException {
        //given
        given(fileManager.getAllUsersFromFiles()).willReturn(Collections.emptyList());

        //when
        userManager.registerUsersFromFiles();

        //then
        assertThat(userManager.getRegisteredUsers()).isEmpty();
    }

    @Test
    void shouldRegisterUsersWhenSomeUsersFoundFromFiles() throws IOException {
        //given
        User user1 = new User("user1", "user_password");
        User user2 = new User("user2", "user_password");

        given(fileManager.getAllUsersFromFiles()).willReturn(List.of(user1, user2));

        //when
        userManager.registerUsersFromFiles();

        //then
        assertThat(userManager.getRegisteredUsers().size()).isEqualTo(2);
    }

    @Test
    void shouldThrowIOExceptionWhileRegisteringWhenFilesAreInWrongFormat() throws IOException {
        //given
        given(fileManager.getAllUsersFromFiles()).willThrow(IOException.class);

        //when
        //then
        assertThrows(IOException.class, () -> userManager.registerUsersFromFiles());
    }

    @Test
    void shouldFindUserWhenUserIsRegistered() {
        //given
        User user = new User("user", "user_password");
        userManager.registerUser(user);

        //when
        User result = userManager.findUser("user");

        //then
        assertNotNull(result);
        assertThat(result).isEqualTo(user);
    }

    @Test
    void shouldNotFindUserWhenUserIsNotRegistered() {
        //given
        User user = new User("user", "user_password");

        //when
        User result = userManager.findUser("user");

        //then
        assertNull(result);
    }

    @Test
    void shouldNotRegisterUserWhenLoginAlreadyRegistered() throws IOException {
        //given
        User user = new User("login", "user_password");
        userManager.registerUser(user);

        //when
        boolean result = userManager.registerUser("login", "password");

        //then
        assertFalse(result);
        assertThat(userManager.getRegisteredUsers().size()).isEqualTo(1);
    }

    @Test
    void shouldNotRegisterUserWhenLoginIsEmpty() throws IOException {
        //given
        //when
        boolean result = userManager.registerUser("", "password");

        //then
        assertFalse(result);
    }

    @Test
    void shouldNotRegisterUserWhenPasswordIsEmpty() throws IOException {
        //given
        //when
        boolean result = userManager.registerUser("login", "");

        //then
        assertFalse(result);
    }

    @Test
    void shouldRegisterUserWhenLoginAndPasswordCorrect() throws IOException {
        //given
        //when
        boolean result = userManager.registerUser("login", "user_password");

        //then
        assertTrue(result);
        assertThat(userManager.getRegisteredUsers().size()).isEqualTo(1);
    }

    @Test
    void shouldNotLoginUserWhenUserNotRegistered() {
        //given
        User user = new User("login", "user_password");
        userManager.registerUser(user);

        //when
        boolean result = userManager.loginUser("new_login", "password");

        //then
        assertFalse(result);
        assertThat(userManager.getLoggedUser()).isNull();
    }

    @Test
    void shouldNotLoginUserWhenPasswordIsInvalid() {
        //given
        User user = new User("login", "user_password");
        userManager.registerUser(user);

        //when
        boolean result = userManager.loginUser("login", "password");

        //then
        assertFalse(result);
        assertThat(userManager.getLoggedUser()).isNull();
    }

    @Test
    void shouldLoginNormalUserWhenAuthorisationDataAreCorrect() {
        //given
        User user = new User("login", "user_password");
        userManager.registerUser(user);

        //when
        boolean result = userManager.loginUser("login", "user_password");

        //then
        assertTrue(result);
        assertThat(userManager.getLoggedUser()).isEqualTo(user);
        assertFalse(userManager.getLoggedUser().isAdmin());
    }

    @Test
    void shouldLoginAdminWhenAuthorisationDataAreCorrect() {
        //given
        User admin = new User("admin", "admin_password", Role.ADMIN);
        userManager.registerUser(admin);

        //when
        boolean result = userManager.loginUser("admin", "admin_password");

        //then
        assertTrue(result);
        assertThat(userManager.getLoggedUser()).isEqualTo(admin);
        assertTrue(userManager.getLoggedUser().isAdmin());
    }

    @Test
    void shouldCorrectlyLogoutUser() {
        //given
        User user = new User("login", "user_password");
        userManager.registerUser(user);
        userManager.loginUser("login", "user_password");

        //when
        userManager.logoutUser();

        //then
        assertThat(userManager.getLoggedUser()).isNull();
    }

    @Test
    void notLoggedUserShouldNotBeAbleToDeleteUser() {
        //given
        User user = new User("login", "password");
        userManager.registerUser(user);

        //when
        boolean result = userManager.deleteUser("login");

        //then
        assertFalse(result);
        assertThat(userManager.getRegisteredUsers()).hasSize(1);
    }

    @Test
    void shouldNotDeleteUserWhenUserNotFound() {
        //given
        User user = new User("login", "password");
        userManager.registerUser(user);
        userManager.loginUser("login", "password");

        //when
        boolean result = userManager.deleteUser("bad-login");

        //then
        assertFalse(result);
        assertNotNull(userManager.getLoggedUser());
        assertThat(userManager.getRegisteredUsers()).hasSize(1);
    }

    @Test
    void normalUserShouldNotBeAbleToDeleteAdmin() {
        //given
        User admin = new User("admin", "admin_password", Role.ADMIN);
        User user = new User("login", "password");
        userManager.registerUser(admin);
        userManager.registerUser(user);
        userManager.loginUser("login", "password");

        //when
        boolean result = userManager.deleteUser("admin");

        //then
        assertFalse(result);
        assertNotNull(userManager.getLoggedUser());
        assertThat(userManager.getRegisteredUsers()).hasSize(2);
    }

    @Test
    void adminShouldNotBeAbleToDeleteAdmin() {
        //given
        User admin = new User("admin", "admin_password", Role.ADMIN);
        User user = new User("login", "password");
        userManager.registerUser(admin);
        userManager.registerUser(user);
        userManager.loginUser("admin", "admin_password");

        //when
        boolean result = userManager.deleteUser("admin");

        //then
        assertFalse(result);
        assertNotNull(userManager.getLoggedUser());
        assertThat(userManager.getRegisteredUsers()).hasSize(2);
    }

    @Test
    void normalUserShouldBeAbleToDeleteNormalUserWhenUserFound() {
        //given
        User admin = new User("admin", "admin_password", Role.ADMIN);
        User user = new User("login", "password");
        userManager.registerUser(admin);
        userManager.registerUser(user);
        userManager.loginUser("login", "password");

        given(fileManager.deleteUserFile(user)).willReturn(true);

        //when
        boolean result = userManager.deleteUser("login");

        //then
        assertTrue(result);
        assertNull(userManager.getLoggedUser());
        assertThat(userManager.getRegisteredUsers()).hasSize(1);
    }

    @Test
    void adminShouldBeAbleToDeleteNormalUserWhenUserFound() {
        //given
        User admin = new User("admin", "admin_password", Role.ADMIN);
        User user = new User("login", "password");
        userManager.registerUser(admin);
        userManager.registerUser(user);
        userManager.loginUser("admin", "admin_password");

        given(fileManager.deleteUserFile(user)).willReturn(true);

        //when
        boolean result = userManager.deleteUser("login");

        //then
        assertTrue(result);
        assertNotNull(userManager.getLoggedUser());
        assertThat(userManager.getRegisteredUsers()).hasSize(1);
    }

    @Test
    void normalUserShouldNotAbleToDeleteAnotherNormalUser() {
        //given
        User user1 = new User("login1", "password1");
        User user2 = new User("login2", "password2");
        userManager.registerUser(user1);
        userManager.registerUser(user2);
        userManager.loginUser("login1", "password1");

        //when
        boolean result = userManager.deleteUser("login2");

        //then
        assertFalse(result);
        assertNotNull(userManager.getLoggedUser());
        assertThat(userManager.getRegisteredUsers()).hasSize(2);
    }

    @Test
    void shouldNotAllowToWriteMessageWhenRecipientNotFound() throws IOException {
        //given
        Message message = new Message();

        User user = new User("login", "password");
        userManager.registerUser(user);
        userManager.loginUser("login", "password");

        //when
        userManager.writeUserMessage(message, "login2");

        //then
        then(fileManager).should(never()).updateUserFile(any(User.class));
    }

    @Test
    void shouldAllowToWriteMessageWhenRecipientFound() throws IOException {
        //given
        Message message = new Message();

        User user1 = new User("login1", "password1");
        User user2 = new User("login2", "password2");
        userManager.registerUser(user1);
        userManager.registerUser(user2);
        userManager.loginUser("login1", "password1");

        //when
        userManager.writeUserMessage(message, "login2");

        //then
        then(fileManager).should().updateUserFile(user2);
    }
}